package com.yosef.LEAP.DTO.scoreDTO;

import com.yosef.LEAP.DTO.userDTO.GetUserDTO;
import com.yosef.LEAP.model.User;

public class GetScoreDTO {
    private int assignmentScore;
    private int finalExamScore;
    private int midExamScore;
    private GetUserDTO user;

    public GetScoreDTO(int assignmentScore, int finalExamScore, int midExamScore, GetUserDTO user) {
        this.assignmentScore = assignmentScore;
        this.finalExamScore = finalExamScore;
        this.midExamScore = midExamScore;
        this.user = user;
    }

    public int getAssignmentScore() {
        return assignmentScore;
    }

    public void setAssignmentScore(int assignmentScore) {
        this.assignmentScore = assignmentScore;
    }

    public int getFinalExamScore() {
        return finalExamScore;
    }

    public void setFinalExamScore(int finalExamScore) {
        this.finalExamScore = finalExamScore;
    }

    public int getMidExamScore() {
        return midExamScore;
    }

    public void setMidExamScore(int midExamScore) {
        this.midExamScore = midExamScore;
    }

    public GetUserDTO getUser() {
        return user;
    }

    public void setUser(GetUserDTO user) {
        this.user = user;
    }
}
