package com.yosef.LEAP.DTO.scoreDTO;

public class CreateScoreDTO {
    private int assignmentScore;
    private int midExamScore;
    private int finalExamScore;
    private int userId;

    public CreateScoreDTO(int assignmentScore, int midExamScore, int finalExamScore, int userId) {
        this.assignmentScore = assignmentScore;
        this.midExamScore = midExamScore;
        this.finalExamScore = finalExamScore;
        this.userId = userId;
    }

    public int getAssignmentScore() {
        return assignmentScore;
    }

    public void setAssignmentScore(int assignmentScore) {
        this.assignmentScore = assignmentScore;
    }

    public int getMidExamScore() {
        return midExamScore;
    }

    public void setMidExamScore(int midExamScore) {
        this.midExamScore = midExamScore;
    }

    public int getFinalExamScore() {
        return finalExamScore;
    }

    public void setFinalExamScore(int finalExamScore) {
        this.finalExamScore = finalExamScore;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
