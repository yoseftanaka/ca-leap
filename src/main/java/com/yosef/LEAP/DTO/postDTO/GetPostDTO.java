package com.yosef.LEAP.DTO.postDTO;

import com.yosef.LEAP.DTO.userDTO.GetUserDTO;

public class GetPostDTO {
    private String message;
    private GetUserDTO getUserDTO;

    public GetPostDTO(String message, GetUserDTO getUserDTO){
        this.message = message;
        this.getUserDTO = getUserDTO;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GetUserDTO getGetUserDTO() {
        return getUserDTO;
    }

    public void setGetUserDTO(GetUserDTO getUserDTO) {
        this.getUserDTO = getUserDTO;
    }
}
