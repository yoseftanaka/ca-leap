package com.yosef.LEAP.DTO.userDTO;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class CreateUserDTO {
    private String name;
    private String username;
    private String password;
    private Date dateOfBirth;

    public CreateUserDTO(String name, String username, String password, Date dateOfBirth) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
