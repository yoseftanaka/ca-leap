package com.yosef.LEAP.exception;

import java.util.Date;

public class ExceptionResponse {
    private String message;
    private String details;
    private Date timer;

    public ExceptionResponse(String message, String details, Date timestamp) {
        this.message = message;
        this.details = details;
        this.timer = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getTimer() {
        return timer;
    }

    public void setTimer(Date timestamp) {
        this.timer = timestamp;
    }
}
