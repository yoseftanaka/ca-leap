package com.yosef.LEAP.exception;

public class ScoreNotFoundException extends RuntimeException {
    public ScoreNotFoundException(String message){
        super(message);
    }
}
