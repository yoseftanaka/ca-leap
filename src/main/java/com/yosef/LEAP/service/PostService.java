package com.yosef.LEAP.service;

import com.yosef.LEAP.DTO.postDTO.GetPostDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {
    public List<GetPostDTO> getAllPostByUser(int user_id);
}
