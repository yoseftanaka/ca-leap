package com.yosef.LEAP.service.impl;

import com.yosef.LEAP.DTO.scoreDTO.CreateScoreDTO;
import com.yosef.LEAP.DTO.scoreDTO.GetScoreDTO;
import com.yosef.LEAP.DTO.userDTO.GetUserDTO;
import com.yosef.LEAP.exception.ScoreNotFoundException;
import com.yosef.LEAP.exception.UserNotFoundExeption;
import com.yosef.LEAP.model.Score;
import com.yosef.LEAP.model.User;
import com.yosef.LEAP.repository.ScoreRepository;
import com.yosef.LEAP.repository.UserRepository;
import com.yosef.LEAP.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ScoreServiceImpl implements ScoreService {

//    Logger logger = new Logger();

    @Autowired
    private ScoreRepository scoreRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<GetScoreDTO> getAllScore() {
        List<GetScoreDTO> scoreList = new ArrayList<>();
        scoreRepository.findAll().forEach(score -> {
            scoreList.add(new GetScoreDTO(score.getAssignmentScore(),
                    score.getFinalExamScore(),
                    score.getMidExamScore(),
                    new GetUserDTO(score.getUser().getName(),score.getUser().getUsername(),score.getUser().getDateOfBirth())));
        });
        return scoreList;
    }

    @Override
    public GetScoreDTO getOneScore(int id) {
        if(findSingleScore(id)!=null){
            Score score = findSingleScore(id);
            return new GetScoreDTO(score.getAssignmentScore(),score.getFinalExamScore(),score.getMidExamScore(),new GetUserDTO(score.getUser().getName(),score.getUser().getUsername(),score.getUser().getDateOfBirth()));
        }
        else return null;
    }

    @Override
    public GetScoreDTO createScore(CreateScoreDTO createScoreDTO) {
        Score score = new Score();
        User user = userRepository.findById(createScoreDTO.getUserId()).get();
        if(user==null){
            throw new UserNotFoundExeption("user with id "+createScoreDTO.getUserId()+" is not found");
        }

        score.setAssignmentScore(createScoreDTO.getAssignmentScore());
        score.setMidExamScore(createScoreDTO.getMidExamScore());
        score.setFinalExamScore(createScoreDTO.getFinalExamScore());
        score.setUser(user);
        scoreRepository.save(score);
        return new GetScoreDTO(score.getAssignmentScore(),score.getFinalExamScore(),score.getMidExamScore(), new GetUserDTO(score.getUser().getName(),score.getUser().getUsername(),score.getUser().getDateOfBirth()));
    }

    @Override
    public GetScoreDTO updateScore(int id, CreateScoreDTO createScoreDTO) {
        Score score = findSingleScore(id);
        if(score==null) throw new ScoreNotFoundException("Score with id "+id+" is not found");
        User user = findSingleUser(createScoreDTO.getUserId());
        if(user==null) throw new UserNotFoundExeption("User "+createScoreDTO.getUserId()+" not found");
        if(score!=null && user != null){
            score.setAssignmentScore(createScoreDTO.getAssignmentScore());
            score.setMidExamScore(createScoreDTO.getMidExamScore());
            score.setFinalExamScore(createScoreDTO.getFinalExamScore());
            score.setUser(user);
            scoreRepository.save(score);
            return new GetScoreDTO(score.getAssignmentScore(),score.getFinalExamScore(),score.getMidExamScore(), new GetUserDTO(score.getUser().getName(),score.getUser().getUsername(),score.getUser().getDateOfBirth()));
        }
        return null;
    }

    @Override
    public String deleteScore(int id) {
        if(findSingleScore(id)!=null){
            scoreRepository.deleteById(id);
            return "Score successfully deleted";
        }
        return "score not found";
    }

    public Score findSingleScore(int id){
        if(scoreRepository.findById(id).isPresent()){
            Score score = scoreRepository.findById(id).get();
            return score;
        }
        else return null;
    }

    public User findSingleUser(int id){
        if (userRepository.findById(id).isPresent()){
            User user = userRepository.findById(id).get();
            return user;
        }
        return null;
    }
}
