package com.yosef.LEAP.service.impl;

import com.yosef.LEAP.DTO.userDTO.CreateUserDTO;
import com.yosef.LEAP.DTO.userDTO.GetUserDTO;
import com.yosef.LEAP.exception.UserNotFoundExeption;
import com.yosef.LEAP.model.User;
import com.yosef.LEAP.repository.UserRepository;
import com.yosef.LEAP.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<GetUserDTO> getAllUser() {
        List<GetUserDTO> userList = new ArrayList<>();
        userRepository.findAll().forEach(user->{
            userList.add(new GetUserDTO(user.getName(), user.getUsername(), user.getDateOfBirth()));
        });
        return userList;
    }

    @Override
    public GetUserDTO getOneUser(int id) {
        if(findSingleUser(id)!=null){
            User user = findSingleUser(id);
            return new GetUserDTO(user.getName() , user.getUsername() , user.getDateOfBirth());
        }
        else return null;
    }

    @Override
    public GetUserDTO createUser(CreateUserDTO createUserDTO){
        User user = new User();
        user.setName(createUserDTO.getName());
        user.setUsername(createUserDTO.getUsername());
        user.setPassword(createUserDTO.getPassword());
        user.setDateOfBirth(createUserDTO.getDateOfBirth());
        userRepository.save(user);
        return new GetUserDTO(user.getName(),user.getUsername(),user.getDateOfBirth());
    }

    @Override
    public GetUserDTO updateUser(int id,CreateUserDTO createUserDTO){
        User user = findSingleUser(id);
        if(user != null){
            user.setName(createUserDTO.getName());
            user.setUsername(createUserDTO.getUsername());
            user.setPassword(createUserDTO.getPassword());
            user.setDateOfBirth(createUserDTO.getDateOfBirth());
            userRepository.save(user);
            return new GetUserDTO(user.getName(),user.getUsername(),user.getDateOfBirth());
        }
        else return null;
    }

    public String deleteUser(int id){
        if(findSingleUser(id)!=null){
            userRepository.deleteById(id);
            return "User successfully deleted";
        }
        throw new UserNotFoundExeption("user with id "+id+" is not found");
    }

    public User findSingleUser(int id){
        if(userRepository.findById(id).isPresent()){
            User user = userRepository.findById(id).get();
            return user;
        }
        else throw new UserNotFoundExeption("user with id "+id+" is not found");
    }


}
