package com.yosef.LEAP.service.impl;

import com.yosef.LEAP.DTO.postDTO.GetPostDTO;
import com.yosef.LEAP.DTO.userDTO.GetUserDTO;
import com.yosef.LEAP.exception.UserNotFoundExeption;
import com.yosef.LEAP.model.User;
import com.yosef.LEAP.repository.PostRepository;
import com.yosef.LEAP.repository.UserRepository;
import com.yosef.LEAP.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<GetPostDTO> getAllPostByUser(int user_id) {
        List<GetPostDTO> postList = new ArrayList<>();
        User user = userRepository.findById(user_id).get();

        if(user==null) throw new UserNotFoundExeption("user with id "+user_id+" is not found");
        postRepository.findAll().forEach((post)->{
            if(post.getUser().getId() == user_id){
                postList.add(new GetPostDTO(post.getMessages(),
                        new GetUserDTO(post.getUser().getName(),
                                post.getUser().getUsername(),
                                post.getUser().getDateOfBirth())));
            }
        });
        return postList;
    }
}
