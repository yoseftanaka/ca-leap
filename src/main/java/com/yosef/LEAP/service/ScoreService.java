package com.yosef.LEAP.service;

import com.yosef.LEAP.DTO.scoreDTO.CreateScoreDTO;
import com.yosef.LEAP.DTO.scoreDTO.GetScoreDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScoreService {
    public List<GetScoreDTO> getAllScore();
    public GetScoreDTO getOneScore(int id);
    public GetScoreDTO createScore(CreateScoreDTO createScoreDTO);
    public GetScoreDTO updateScore(int id, CreateScoreDTO createScoreDTO);
    public String deleteScore(int id);
}
