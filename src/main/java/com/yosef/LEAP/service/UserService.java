package com.yosef.LEAP.service;

import com.yosef.LEAP.DTO.userDTO.CreateUserDTO;
import com.yosef.LEAP.DTO.userDTO.GetUserDTO;

import java.util.List;

public interface UserService {
    public List<GetUserDTO> getAllUser();
    public GetUserDTO getOneUser(int id);
    public GetUserDTO createUser(CreateUserDTO createUserDTO);
    public GetUserDTO updateUser(int id, CreateUserDTO createUserDTO);
    public String deleteUser(int id);
}
