package com.yosef.LEAP.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yosef.LEAP.validator.ScoreIsValid;

import javax.persistence.*;

@Entity
@Table(name = "scores")
public class Score extends BaseModel{

    @ScoreIsValid(message = "Assignment score must be between 0 and 100")
    @Column(name = "assignment_score")
    private int assignmentScore;

    @ScoreIsValid(message = "Mid exam score must be between 0 and 100")
    @Column(name = "mid_exam_score")
    private int midExamScore;

    @ScoreIsValid(message = "Final exam score must be between 0 and 100")
    @Column(name = "final_exam_score")
    private int finalExamScore;

    @OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Score(int assignmentScore, int midExamScore, int finalExamScore) {
        this.assignmentScore = assignmentScore;
        this.midExamScore = midExamScore;
        this.finalExamScore = finalExamScore;
    }

    public Score(){}

    public int getAssignmentScore() {
        return assignmentScore;
    }

    public void setAssignmentScore(int assignmentScore) {
        this.assignmentScore = assignmentScore;
    }

    public int getMidExamScore() {
        return midExamScore;
    }

    public void setMidExamScore(int midExamScore) {
        this.midExamScore = midExamScore;
    }

    public int getFinalExamScore() {
        return finalExamScore;
    }

    public void setFinalExamScore(int finalExamScore) {
        this.finalExamScore = finalExamScore;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Score{" +
                "assignmentScore=" + assignmentScore +
                ", midExamScore=" + midExamScore +
                ", finalExamScore=" + finalExamScore +
                ", user=" + user +
                '}';
    }
}
