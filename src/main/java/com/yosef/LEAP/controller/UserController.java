package com.yosef.LEAP.controller;

import com.yosef.LEAP.DTO.userDTO.CreateUserDTO;
import com.yosef.LEAP.DTO.userDTO.GetUserDTO;
import com.yosef.LEAP.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value =  "/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<GetUserDTO>> getAllUser(){
        return new ResponseEntity<>(userService.getAllUser(),HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<GetUserDTO> getOneUser(@PathVariable int id){
        return new ResponseEntity<>(userService.getOneUser(id),HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GetUserDTO> createUser(@Valid @RequestBody CreateUserDTO createUserDTO){
        return new ResponseEntity<>(userService.createUser(createUserDTO),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<GetUserDTO> updateUser(@PathVariable int id ,@RequestBody CreateUserDTO createUserDTO){
        return new ResponseEntity<>(userService.updateUser(id,createUserDTO),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteUser(@PathVariable int id){
        return new ResponseEntity<>(userService.deleteUser(id),HttpStatus.OK);
    }
}
