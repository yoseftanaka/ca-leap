package com.yosef.LEAP.controller;

import com.yosef.LEAP.DTO.postDTO.GetPostDTO;
import com.yosef.LEAP.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/posts")
public class PostController {

    @Autowired
    PostService postService;

    @GetMapping(value = "/{user_id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<GetPostDTO>> getPostByUser(@PathVariable int user_id){
        return new ResponseEntity<>(postService.getAllPostByUser(user_id), HttpStatus.OK);
    }
}
