package com.yosef.LEAP.controller;

import com.yosef.LEAP.DTO.scoreDTO.CreateScoreDTO;
import com.yosef.LEAP.DTO.scoreDTO.GetScoreDTO;
import com.yosef.LEAP.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/scores")
public class ScoreController {

    @Autowired
    private ScoreService scoreService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<GetScoreDTO>> getAllScore(){
        return new ResponseEntity<>(scoreService.getAllScore(),HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<GetScoreDTO> getOneScore(@PathVariable int id){
        return new ResponseEntity<>(scoreService.getOneScore(id),HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GetScoreDTO> createScore(@Valid @RequestBody CreateScoreDTO createScoreDTO){
        return new ResponseEntity<>(scoreService.createScore(createScoreDTO),HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<GetScoreDTO> updateScore(@PathVariable int id, @Valid @RequestBody CreateScoreDTO createScoreDTO){
        return new ResponseEntity<>(scoreService.updateScore(id,createScoreDTO),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteScore(@PathVariable int id){
        return new ResponseEntity<>(scoreService.deleteScore(id),HttpStatus.OK);
    }
}
