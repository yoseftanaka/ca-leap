package com.yosef.LEAP.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ScoreValidator implements ConstraintValidator<ScoreIsValid, Integer> {

    @Override
    public void initialize(ScoreIsValid constraintAnnotation) {

    }

    @Override
    public boolean isValid(Integer scoreField, ConstraintValidatorContext constraintValidatorContext) {
        return scoreField!=null && scoreField>-1 && scoreField<101;
    }
}
