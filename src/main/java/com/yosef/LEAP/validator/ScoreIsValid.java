package com.yosef.LEAP.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ScoreValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ScoreIsValid {
    String message() default "score must be mare than -1 and less than 100";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
